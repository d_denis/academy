<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["ITEMS"]):?>
<script>
	$().ready(function(){
		$(function(){
			$('#slides').slides({
				preload: false,
				generateNextPrev: false,
				autoHeight: true,
				play: 4000,
				effect: 'fade'
			});
		});
	});
</script>



<div class="sl_slider" id="slides">
	<div class="slides_container">

		<?foreach($arResult["ITEMS"] as $arItem):?>
        <?$ITEM_ID = $arItem["PROPERTIES"]['LINK']['VALUE'];?>

		<div>
			<div>

                <?if(is_array($arItem["DETAIL_PICTURE"])):?>
                <?if($arResult["CAT_ELEM"][$ITEM_ID]["DETAIL_PAGE_URL"]):?>
                    <a title="<?=$arResult["CAT_ELEM"][$ITEM_ID]["NAME"]?>" href="<?=$arResult["CAT_ELEM"][$ITEM_ID]["DETAIL_PAGE_URL"]?>">
                        <img src="<?=$arItem["DETAIL_PICTURE"]["src"]?>" alt="" />
                    </a>
                <?else:?>
                    <img src="<?=$arItem["DETAIL_PICTURE"]["src"]?>" alt="" />
                <?endif;?>
                <?endif;?>
                <?if($arItem["NAME"]):?>
				<h2>
                    <?if($arResult["CAT_ELEM"][$ITEM_ID]["DETAIL_PAGE_URL"]):?>
                    <a title="<?=$arResult["CAT_ELEM"][$ITEM_ID]["NAME"]?>" href="<?=$arResult["CAT_ELEM"][$ITEM_ID]["DETAIL_PAGE_URL"]?>">
                        <?echo $arItem["NAME"]?>
                    </a>
                    <?else:?>
                    <?echo $arItem["NAME"]?>
                    <?endif;?>
                </h2>
                <?endif;?>
                <?if($arResult["CAT_ELEM"][$ITEM_ID]["NAME"] or $arResult["CAT_ELEM"][$ITEM_ID]["PROPERTY_PRICE_VALUE"]):?>
				<p>
                    <?if($arResult["CAT_ELEM"][$ITEM_ID]["NAME"]):?>
                    <?=$arResult["CAT_ELEM"][$ITEM_ID]["NAME"]?>
                    <?endif;?>


                    <?if($arResult["CAT_ELEM"][$ITEM_ID]["PROPERTY_PRICE_VALUE"]):?>
                    <?=GetMessage('ONLY_FOR')?>
                    <?=$arResult["CAT_ELEM"][$ITEM_ID]["PROPERTY_PRICE_VALUE"]?> <?=GetMessage('RUB')?>
                    <?endif;?>
                </p>
                <?endif;?>
                <?if($arResult["CAT_ELEM"][$ITEM_ID]["DETAIL_PAGE_URL"]):?>
                <a title="<?=$arResult["CAT_ELEM"][$ITEM_ID]["NAME"]?>" href="<?=$arResult["CAT_ELEM"][$ITEM_ID]["DETAIL_PAGE_URL"]?>" class="sl_more">
                    <?=GetMessage('DETAILS')?>
                </a>
                <?endif;?>
			</div>
		</div>
		<?endforeach;?>
	</div>
</div>
<?endif;?>
