<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arTempID = array();
foreach ($arResult["ITEMS"] as $ID => $ITEM) {
    $arTempID[] = $ITEM["PROPERTIES"]["LINK"]["VALUE"];
    $arResult["ITEMS"][$ID]["DETAIL_PICTURE"] = CFile::ResizeImageGet($ITEM['DETAIL_PICTURE'], array('width'=>$arParams['LIST_PREV_PICT_W'], 'height'=>$arParams['LIST_PREV_PICT_H']), BX_RESIZE_IMAGE_PROPORTIONAL, true);
}
$arResult["CAT_ELEM"] = array();
if (count($arTempID)) {

    $arFilter = array(
        "IBLOCK_ID" => IBLOCK_CAT_ID,
        "ACTIVE" => "Y",
        "ID" => $arTempID
    );
    $arSelect = array("NAME", "PROPERTY_PRICE", "DETAIL_PAGE_URL");

    $res = CIBlockElement::GetList(
        array(),
        $arFilter,
        false,
        false,
        $arSelect
    );


    while ($ITEM = $res->GetNext()) {
        $arResult["CAT_ELEM"][$ITEM["ID"]] = $ITEM;
    }
    
}
?>
