<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$arResult["MATERIALS_CNT"] = array();

$arFilter = array(
    "IBLOCK_ID" => IBLOCK_CAT_ID,
    "ACTIVE" => "Y",
    "IBLOCK_SECTION_ID" => $arResult['ID']
);
$arSelect = array("PROPERTY_MATERIAL");
$arGroupBy = array("PROPERTY_MATERIAL");
$res = CIBlockElement::GetList(
    array(),
    $arFilter,
    $arGroupBy,
    false,
    $arSelect
);

while ($ITEM = $res->GetNext()) {
    array_push($arResult["MATERIALS_CNT"], $ITEM);
}