<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html lang=”<?=LANGUAGE_ID;?>-<?=strtoupper(LANGUAGE_ID);?>”>
<head>
    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle();?></title>
    <?
    $APPLICATION->AddHeadScript("/bitrix/templates/.default/js/jquery-1.8.2.min.js");
    $APPLICATION->SetAdditionalCSS("/bitrix/templates/.default/template_styles.css");
    $APPLICATION->AddHeadScript("/bitrix/templates/.default/js/functions.js");
    ?>


    <link rel="shortcut icon" type="image/x-icon" href="/bitrix/templates/.default/favicon.ico"/>

    <!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<div class="wrap">
    <?include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/templates/.default/include/header.php");?>


    <!--- // end header area --->
    <?$APPLICATION->IncludeComponent(
        "bitrix:breadcrumb",
        "nav",
        Array(
            "PATH" => "",
            "SITE_ID" => "s1",
            "START_FROM" => "0"
        )
    );?>
    <div class="main_container page">
        <div class="mn_container">
            <div class="mn_content">
                <div class="main_post">
                    <div class="main_title">
                        <?$APPLICATION->ShowViewContent("news_detail_date");?>
                        <p class="title"><?$APPLICATION->ShowTitle(false);
                        $APPLICATION->ShowViewContent("rating")?></p>
                    </div>
                    <!-- workarea -->