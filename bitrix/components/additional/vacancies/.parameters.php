<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;
if (!CModule::IncludeModule("form"))
	return;
$arIBlockType = CIBlockParameters::GetIBlockTypes();


$arIBlock=array(
	"-" => GetMessage("IBLOCK_ANY"),
);
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch())
{
	$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}
$arProperties = array();
$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID"]));
while ($prop_fields = $properties->GetNext())
{
    $arProperties[$prop_fields['CODE']] = $prop_fields["CODE"]." - ".$prop_fields['NAME'];
}
$arrForms = array();
$rsForm = CForm::GetList($by='s_sort', $order='asc', array("SITE" => $_REQUEST["site"]), $v3);
while ($arForm = $rsForm->Fetch())
{
    $arrForms[$arForm["ID"]] = "[".$arForm["ID"]."] ".$arForm["NAME"];
}
$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
        "VARIABLE_ALIASES" => Array(
            "ID" => Array("NAME" => GetMessage("VAC_ELEMENT_ID_DESC")),

        ),
        "SEF_MODE" => Array(
            "vacancies" => array(
                "NAME" => GetMessage("T_IBLOCK_SEF_PAGE_VAC"),
                "DEFAULT" => "vacancies/",
                "VARIABLES" => array(),
            ),
            "resume" => array(
                "NAME" => GetMessage("T_IBLOCK_SEF_PAGE_VAC_SECTION"),
                "DEFAULT" => "#VACANT_ID#/resume/",
                "VARIABLES" => array(),
            ),
            "vacancy" => array(
                "NAME" => GetMessage("T_IBLOCK_SEF_PAGE_VAC_DETAIL"),
                "DEFAULT" => "#VACANT_ID#/",
                "VARIABLES" => array("ID"),
            ),
        ),
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlockType,
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_IBLOCK"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlock,
			"REFRESH" => "Y",
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
		"CACHE_GROUPS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => GetMessage("CP_BPR_CACHE_GROUPS"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
        "IBLOCKS_PROP" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("IBLOCKS_PROP"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arProperties,
            "REFRESH" => "Y",
        ),
        "WEB_FORM_ID" => array(
            "NAME" => GetMessage("FORM_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arrForms,
            "ADDITIONAL_VALUES"	=> "Y",
            "DEFAULT" => "={\$_REQUEST[WEB_FORM_ID]}",
            "PARENT" => "DATA_SOURCE",
        ),
	),
);
?>
