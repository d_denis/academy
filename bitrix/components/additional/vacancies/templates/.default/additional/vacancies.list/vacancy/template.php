<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>


<?foreach ($arResult['VACANCY'] as $key => $arItem){?>

    <?
    $this->AddEditAction($key, $arResult[$key]["LINKS"]["ELEM"]["ADD"]["LINK"], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_ADD"));
    $this->AddEditAction($key, $arResult[$key]["LINKS"]["SECTION"]["ADD"]["LINK"], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "SECTION_ADD"));
    $this->AddEditAction($key, $arResult[$key]["LINKS"]["SECTION"]["EDIT"]["LINK"], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "SECTION_EDIT"));
    $this->AddDeleteAction($key, $arResult[$key]["LINKS"]["SECTION"]["DELETE"]["LINK"], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_SECTION_DELETE_CONFIRM')));
    ?>
    <div class="vc_content" id="<?=$this->GetEditAreaId($key);?>">
        <?if($arResult['SECTIONS'][$key]){?>
            <h2 onclick="showFun(<?=$key?>)" id="<?=$arResult['SECTIONS'][$key]?>"><?=$arResult['SECTIONS'][$key]?> (<?=count($arItem)?>)</h2>
            <ul id="<?=$key?>" style="display: none">
                <?foreach ($arItem as $item){?>
                    <?
                    $this->AddEditAction($item['ID'], $item['LINKS']['EDIT']['LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($item['ID'], $item['LINKS']['DELETE']['LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <?if($item['NAME']){?>
                        <div id="<?=$this->GetEditAreaId($item['ID']);?>">
                            <li class="open"><h3><?=$item['NAME']?></h3>
                                <ul>
                                    <li>
                                        <?if($item['PROPERTY_EDUCATION_VALUE']){?>
                                            <strong><?=GetMessage('DEMAND')?>:</strong>
                                            <br/><?=GetMessage('EDUCATION')?> - <?=$item['PROPERTY_EDUCATION_VALUE']?>
                                            <br/>
                                        <?}?>
                                        <?if($item['PROPERTY_EXPERIENCE_VALUE']){?>
                                            <br/><strong><?=GetMessage('EXPERIENCE')?>:</strong>
                                            <br/><?=GetMessage('EXPERIENCE')?> - <?=$item['PROPERTY_EXPERIENCE_VALUE']?>
                                            <br/>
                                        <?}?>
                                        <?if($item['PROPERTY_SCHEDULE_VALUE']){?>
                                            <br/><strong><?=GetMessage('WORK_TIME')?>:</strong>
                                            <br/><?=GetMessage('WORK_TIME')?> - <?=$item['PROPERTY_SCHEDULE_VALUE']?>
                                        <?}?>
                                    </li>
                                </ul>
                                <?if($item["DETAIL_PAGE_URL"]){?>
                                    <a href="<?=$item["DETAIL_PAGE_URL"];?>"><?=GetMessage('DETAILS')?></a>
                                <?}?>
                            </li>
                        </div>
                    <?}?>
                <?}?>
            </ul>
        <?}?>
    </div>
<?}?>


