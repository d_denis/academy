<?
$APPLICATION->IncludeComponent(
    "additional:vacancies.list",
    "vacancy",
    Array(
        "CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
        "CACHE_TIME" => $arParams['CACHE_TIME'],
        "CACHE_TYPE" => $arParams['CACHE_TYPE'],
        "COMPONENT_TEMPLATE" => "vacancy",
        "DETAIL_URL" => "",
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "IBLOCKS_PROP" => array(0=>"SCHEDULE",1=>"EDUCATION",2=>"EXPERIENCE",),
        "IBLOCK_ID" => $arParams['IBLOCK_ID'],
        "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
        "MEDIA_PROPERTY" => "",
        "PARENT_SECTION" => "",
        "SEARCH_PAGE" => "/search/",
        "SLIDER_PROPERTY" => "",
        "TEMPLATE_THEME" => "blue",
        "USE_RATING" => "N",
        "USE_SHARE" => "N",
    ),
      $component
);?>