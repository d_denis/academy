<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("iblock"))
    return;

$arFilter = array(
    "IBLOCK_ID" => IBLOCK_VACANCY_ID,
    "ACTIVE" => "Y",
    "ID" => array($arParams['VARIABLE_ALIASES']['VACANT_ID'])
);
$arSelect = array("NAME");

$res = CIBlockElement::GetList(
    array(),
    $arFilter,
    false,
    false,
    $arSelect
);
$res = $res->Fetch();

foreach ($arResult['arQuestions'] as $key => $quest) {
    if ($quest['ID'] == VACANCY_QUEST_ID) {
        // Определяем начало тега input
        $idx = strpos($arResult['QUESTIONS'][$key]['HTML_CODE'], "input") + 6;
        // Вставляем в тег значение readonly
        $arResult['QUESTIONS'][$key]['HTML_CODE'] = substr(
            $arResult['QUESTIONS'][$key]['HTML_CODE'], 0, $idx)."readonly ".substr(
                $arResult['QUESTIONS'][$key]['HTML_CODE'], $idx, strlen($arResult['QUESTIONS'][$key]['HTML_CODE']));
        // Если есть название вакансии добавляем в поле id==name
        if ($res["NAME"]) {
            $arResult['QUESTIONS'][$key]['VALUE'] = $arParams['VARIABLE_ALIASES']['VACANT_ID'] . "==" . $res['NAME'];
        }
        // Иначе выдаем ошибку
        else {
            $arResult['QUESTIONS'][$key]['VALUE'] = 'Такой вакансии не существует.';
        }
        // Определяем начало значения value в теге input
        $idx = strpos($arResult['QUESTIONS'][$key]['HTML_CODE'], "value=\"\"") + 7;
        // Вставляем в value сообщение
        $arResult['QUESTIONS'][$key]['HTML_CODE'] = substr($arResult['QUESTIONS'][$key]['HTML_CODE'], 0, $idx).
            $arResult['QUESTIONS'][$key]['VALUE'].substr($arResult['QUESTIONS'][$key]['HTML_CODE'], $idx,
                strlen($arResult['QUESTIONS'][$key]['HTML_CODE']));
        break;
    }
}
