<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
// Устанавливаем время кэша, если оно не задано
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

// Приводим ID инфоблока к int
$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);

$arDefaultUrlTemplates404 = array(
    "vacancies" => "",
    "resume" => "#VACANT_ID#/resume/",
    "vacancy" => "#VACANT_ID#/"
);

$arDefaultVariableAliases404 = array("vacancy"=>array("VACANT_ID"=>"ID"));

$arDefaultVariableAliases = array("VACANT_ID"=>"ID");

$arComponentVariables = array(
    "VACANT_ID",
    "RESUME"
);
$componentPage = "";

if($arParams["SEF_MODE"] == "Y")
{
    $arVariables = array();

    $arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
    $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);


    $componentPage = CComponentEngine::ParseComponentPath(
        $arParams["SEF_FOLDER"],
        $arUrlTemplates,
        $arVariables
    );
    $resume_url = "";
    if(!$componentPage)
    {
        $componentPage = "vacancies";
    }
    elseif ($componentPage == "vacancy"){
        $resume_url = $arParams['SEF_FOLDER']."vacancies/".$arVariables['VACANT_ID']."/resume/";
    }


    CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

    $arResult = array(
        "FOLDER" => $arParams["SEF_FOLDER"],
        "URL_TEMPLATES" => $arUrlTemplates,
        "VARIABLES" => $arVariables,
        "ALIASES" => $arVariableAliases,
        "RESUME_URL" => $resume_url
    );
}
else
{
    $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
    CComponentEngine::initComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);
    $resume_url = "";

    if(isset($arVariables["VACANT_ID"]) && intval($arVariables["VACANT_ID"]) > 0 && isset($arVariables["RESUME"]) && $arVariables["RESUME"] == "Y")
        $componentPage = "resume";
    elseif(isset($arVariables["VACANT_ID"]) && intval($arVariables["VACANT_ID"]) > 0) {
        $componentPage = "vacancy";
        $resume_url = $APPLICATION->GetCurPage()."?".$arVariableAliases["VACANT_ID"]."=".$arVariables["VACANT_ID"]."&RESUME=Y";
    }
    else
        $componentPage = "vacancies";


    $arResult = array(
        "FOLDER" => "/company/",
        "URL_TEMPLATES" => array(
            "vacancies" => htmlspecialcharsbx($APPLICATION->GetCurPage()),
            "resume" => htmlspecialcharsbx($APPLICATION->GetCurPage()."?".$arVariableAliases["VACANT_ID"]."=#ID#"."&RESUME=Y"),
            "vacancy" => htmlspecialcharsbx($APPLICATION->GetCurPage()."?".$arVariableAliases["VACANT_ID"]."=#ID#")
        ),
        "VARIABLES" => $arVariables,
        "ALIASES" => $arVariableAliases,
        "RESUME_URL" => $resume_url
    );
}




$this->IncludeComponentTemplate($componentPage);


?>
