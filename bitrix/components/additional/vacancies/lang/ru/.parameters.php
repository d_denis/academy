<?
$MESS["IBLOCK_DETAIL_URL"] = "URL, ведущий на страницу с содержимым элемента раздела";
$MESS["IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["IBLOCK_IBLOCK"] = "Инфоблок";
$MESS["IBLOCK_ANY"] = "(любой)";
$MESS["FORM_ID"] = "ID веб-формы";
$MESS["IBLOCK_SECTION_ID"] = "ID раздела";
$MESS["CP_BPR_CACHE_GROUPS"] = "Учитывать права доступа";
$MESS["IBLOCKS_PROP"] = "Какие свойства показывать";
$MESS["BN_P_SECTION_ID_DESC"] = "Идентификатор раздела";
$MESS["VAC_ELEMENT_ID_DESC"] = "Идентификатор вакансии";
$MESS["T_IBLOCK_SEF_PAGE_VAC"] = "Страница общего списка";
$MESS["T_IBLOCK_SEF_PAGE_VAC_SECTION"] = "Страница раздела";
$MESS["T_IBLOCK_SEF_PAGE_VAC_DETAIL"] = "Страница детального просмотра";
?>