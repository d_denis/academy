<?
$MESS["IBLOCK_DETAIL_URL"] = "URL of the page with the detail contents";
$MESS["IBLOCK_TYPE"] = "Type of information block";
$MESS["IBLOCK_IBLOCK"] = "Information block";
$MESS["IBLOCK_ANY"] = "(any)";
$MESS["FORM_ID"] = "ID web-form";
$MESS["IBLOCK_SECTION_ID"] = "Section ID";
$MESS["CP_BPR_CACHE_GROUPS"] = "Respect Access Permissions";
$MESS["IBLOCKS_PROP"] = "What's property to use";
$MESS["BN_P_SECTION_ID_DESC"] = "Section ID";
$MESS["VAC_ELEMENT_ID_DESC"] = "Vacancy ID";
$MESS["T_IBLOCK_SEF_PAGE_VAC"] = "List page";
$MESS["T_IBLOCK_SEF_PAGE_VAC_SECTION"] = "Section page";
$MESS["T_IBLOCK_SEF_PAGE_VAC_DETAIL"] = "Detailed view page";
?>