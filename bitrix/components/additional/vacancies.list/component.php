<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
// Устанавливаем время кэша, если оно не задано
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

// Приводим ID инфоблока к int
$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);


if($arParams['IBLOCK_ID'] > 0 && $this->StartResultCache(false, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups())))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	//SELECT
	$bufarSelect = array();
    foreach ($arParams['IBLOCKS_PROP'] as $prop){
		array_push($bufarSelect, "PROPERTY_".$prop);
    }
	$arSelect = array(
		"ID",
		"IBLOCK_ID",
		"CODE",
		"IBLOCK_SECTION_ID",
		"NAME",
		"DETAIL_PAGE_URL",
	);
    $arSelect = array_merge($arSelect, $bufarSelect);

	//WHERE
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE"=>"Y",
	);

	//EXECUTE
	$rsIBlockElement = CIBlockElement::GetList(Array("sort"=>"asc"), $arFilter, false, false, $arSelect);
	$rsIBlockElement->SetUrlTemplates($arParams["DETAIL_URL"]);

    $items = GetIBlockSectionList($arParams["IBLOCK_ID"], 0, Array("sort"=>"asc"), false);
	while ($item = $items->GetNext()) {
		// Получаем и формируем "Эримитаж" разделов
        $arButtons = CIBlock::GetPanelButtons(
            $arParams["IBLOCK_ID"],
            0,
            $item["ID"],
            array()
        );
        $arResult[$item["ID"]]["LINKS"]["ELEM"]["ADD"]["LINK"] = $arButtons["edit"]["add_element"]["ACTION_URL"];
        $arResult[$item["ID"]]["LINKS"]["ELEM"]["ADD"]["TEXT"] = $arButtons["edit"]["add_element"]["TEXT"];

        $arResult[$item["ID"]]["LINKS"]["SECTION"]["ADD"]["LINK"] = $arButtons["edit"]["add_section"]["ACTION_URL"];
        $arResult[$item["ID"]]["LINKS"]["SECTION"]["ADD"]["TEXT"] = $arButtons["edit"]["add_section"]["TEXT"];

        $arResult[$item["ID"]]["LINKS"]["SECTION"]["EDIT"]["LINK"] = $arButtons["edit"]["edit_section"]["ACTION_URL"];
        $arResult[$item["ID"]]["LINKS"]["SECTION"]["EDIT"]["TEXT"] = $arButtons["edit"]["edit_section"]["TEXT"];

        $arResult[$item["ID"]]["LINKS"]["SECTION"]["DELETE"]["LINK"] = $arButtons["edit"]["delete_section"]["ACTION_URL"];
        $arResult[$item["ID"]]["LINKS"]["SECTION"]["DELETE"]["TEXT"] = $arButtons["edit"]["delete_section"]["TEXT"];

        $arResult["SECTIONS"][$item["ID"]] = $item['NAME'];
    }

	$arResult["IBLOCK_ID"] = $arParams["IBLOCK_ID"];

    while ($elem = $rsIBlockElement->GetNext()){
    	// Собираем и формируем "Эрмитаж" элементов
        $arButtons = CIBlock::GetPanelButtons(
            $elem["IBLOCK_ID"],
            $elem["ID"],
            0,
            array("SECTION_BUTTONS"=>false, "SESSID"=>false)
        );
        $elem["LINKS"]["EDIT"]["LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
        $elem["LINKS"]["EDIT"]["TEXT"] = $arButtons["edit"]["edit_element"]["TEXT"];
        $elem["LINKS"]["DELETE"]["LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
        $elem["LINKS"]["DELETE"]["TEXT"] = $arButtons["edit"]["delete_element"]["TEXT"];

		$arResult['VACANCY'][$elem["IBLOCK_SECTION_ID"]][] = $elem;
	}
	// Формируем кэш ключи
    if(!empty($arResult)) {
        $this->SetResultCacheKeys(array());
        $this->IncludeComponentTemplate();
    }
    else
    {
        $this->AbortResultCache();
    }

}
?>
