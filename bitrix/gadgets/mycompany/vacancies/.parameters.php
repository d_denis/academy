<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("form"))
	return false;

$arForms = array("" => GetMessage("GD_RESUME_EMPTY"));

$arFilter = Array(
    "SITE" => array("s1")
);
$is_filtered = true;
$dbForm = CForm::GetList($by="s_id", $order="desc", $arFilter, $is_filtered);

while($arForm = $dbForm->GetNext())
    //dump($arForm);
	$arForms[$arForm["ID"]] = "[".$arForm["ID"]."] ".$arForm["NAME"];


$arParameters = Array(
	"PARAMETERS"=> Array(
		"FORM_ID" => Array(
			"NAME" => GetMessage("GD_RESUME_FORM_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arForms,
			"MULTIPLE" => "N",
			"DEFAULT" => '',
			"REFRESH" => "Y",
		),
	),
	"USER_PARAMETERS"=> Array(
        "FORM_ID" => Array(
            "NAME" => GetMessage("GD_RESUME_FORM_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arForms,
            "MULTIPLE" => "N",
            "DEFAULT" => '',
            "REFRESH" => "Y",
        ),
		"URL_SHOW_RESUME_TODAY" => array(
			"NAME" => GetMessage("GD_URL_SHOW_RESUME_TODAY"),
			"TYPE" => "STRING",
			"DEFAULT" => '/bitrix/admin/form_result_list.php?WEB_FORM_ID=#WEB_FORM_ID#&set_filter=Y&find_date_create_1_FILTER_PERIOD=day&find_date_create_1_FILTER_DIRECTION=current&find_date_create_1=#FIND_DATE_C_1#&find_date_create_2=#FIND_DATE_C_2#',
		),
		"URL_SHOW_RESUME_ALL" => array(
			"NAME" => GetMessage("GD_URL_SHOW_RESUME_ALL"),
            "TYPE" => "STRING",
            "DEFAULT" => '/bitrix/admin/form_result_list.php?WEB_FORM_ID=#WEB_FORM_ID#&del_filter=Y',
		),
		
	),
);

?>