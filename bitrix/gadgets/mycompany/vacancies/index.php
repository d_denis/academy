<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?

#
# INPUT PARAMS
#
$arGadgetParams["FORM_ID"] = intval($arGadgetParams["FORM_ID"]);
if ($arGadgetParams["FORM_ID"] <= 0)
   return false;

if (empty($arGadgetParams["URL_SHOW_RESUME_TODAY"]) or substr_count($arGadgetParams["URL_SHOW_RESUME_TODAY"], '#WEB_FORM_ID#') == 0) {
    $arGadgetParams["URL_SHOW_RESUME_TODAY"] = '/bitrix/admin/form_result_list.php?WEB_FORM_ID=#WEB_FORM_ID#&set_filter=Y&find_date_create_1_FILTER_PERIOD=day&find_date_create_1_FILTER_DIRECTION=current&find_date_create_1=#FIND_DATE_C_1#&find_date_create_2=#FIND_DATE_C_2#';
}

$arGadgetParams["URL_SHOW_RESUME_TODAY"] = str_replace("#WEB_FORM_ID#", $arGadgetParams["FORM_ID"], $arGadgetParams["URL_SHOW_RESUME_TODAY"]);
$arGadgetParams["URL_SHOW_RESUME_TODAY"] = str_replace("#FIND_DATE_C_1#", CURRENT_DAY, $arGadgetParams["URL_SHOW_RESUME_TODAY"]);
$arGadgetParams["URL_SHOW_RESUME_TODAY"] = str_replace("#FIND_DATE_C_2#", CURRENT_DAY, $arGadgetParams["URL_SHOW_RESUME_TODAY"]);

if (empty($arGadgetParams["URL_SHOW_RESUME_ALL"]) or substr_count($arGadgetParams["URL_SHOW_RESUME_ALL"], '#WEB_FORM_ID#') == 0) {
    $arGadgetParams["URL_SHOW_RESUME_ALL"] = '/bitrix/admin/form_result_list.php?WEB_FORM_ID=#WEB_FORM_ID#&del_filter=Y';
}

$arGadgetParams["URL_SHOW_RESUME_ALL"] = str_replace("#WEB_FORM_ID#", $arGadgetParams["FORM_ID"], $arGadgetParams["URL_SHOW_RESUME_ALL"]);


#
# CACHE
#
$obCache = new CPageCache;
$cacheTime = 5*60;
$cacheId = $arGadgetParams["FORM_ID"].$arGadgetParams["URL_SHOW_RESUME_TODAY"].$arGadgetParams["URL_SHOW_RESUME_ALL"];

if($obCache->StartDataCache($cacheTime, $cacheId, "/")):
	if(!CModule::IncludeModule("form"))
	{
		ShowError(GetMessage("FORM_MODULE_NOT_INSTALLED"));
		return;
	}
    $is_filtered = true;
    $todayElement = CFormResult::GetList(
        $arGadgetParams["FORM_ID"],
        ($by="s_timestamp"),
        ($order="desc"),
        array(
            "DATE_CREATE_1" => CURRENT_DAY,
            "DATE_CREATE_2" => CURRENT_DAY,
        ),
        $is_filtered
    );
	$allElements = CFormResult::GetCount($arGadgetParams["FORM_ID"]);
	?>
    <div>
        <p><?=GetMessage('ALL_RESUME')?><a href="<?=$arGadgetParams["URL_SHOW_RESUME_ALL"]?>"><?=$allElements?></a></p>
        <br>
        <p><?=GetMessage('TODAY_RESUME')?><a href="<?=$arGadgetParams["URL_SHOW_RESUME_TODAY"]?>"><?=$todayElement->SelectedRowsCount()?></a></p>
    </div>
    <?

	$obCache->EndDataCache();
endif;

?>