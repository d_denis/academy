<?
if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/constants.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/constants.php");
}
if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/functions.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/functions.php");
}
if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/agent.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/agent.php");
}
if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/event_handlers/CIBlockHandler.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/event_handlers/CIBlockHandler.php");
}
if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/event_handlers/MainHandler.php")) {
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/event_handlers/MainHandler.php");
}
?>
