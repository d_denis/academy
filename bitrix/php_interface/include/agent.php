<?

function AgentCheckActiveTo()
{
    global $DB;
	
	if(CModule::IncludeModule("iblock"))
	{
        $arSelect = Array("ID", "NAME");
        $arFilter = Array("IBLOCK_ID" => IBLOCK_STOCK_ID, "ACTIVE" => "Y", '<=DATE_ACTIVE_TO' => date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime()));
        $rsResCat = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        $arItems = array();
        while ($arItemCat = $rsResCat->GetNext()) {
            $arItems[] = $arItemCat;
        }
	

	
		if(count($arItems) > 0)
		{
            CEventLog::Add(array(
                "SEVERITY" => "SECURITY",
                "AUDIT_TYPE_ID" => "CHECK_STOCK",
                "MODULE_ID" => "iblock",
                "ITEM_ID" => "",
                "DESCRIPTION" => "Количество акций с истекшей датой: ".count($arItems),
            ));

			$arFilter = Array(
					"GROUPS_ID" => Array(GROUP_ADMIN_ID)
			);
			$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter);
			$arEmail = array();
			while($arResUser = $rsUsers->GetNext())
			{
				$arEmail[] = $arResUser["EMAIL"];
			}

			if(count($arEmail) > 0)
			{
				$arEventFields = array(
						"AMOUNT" => count($arItems),
						"EMAIL" => implode(", ", $arEmail),
				);
				CEvent::Send("CHECK_ACTIVE_TO", MY_SITE_ID, $arEventFields);
			}
		}
	}
	
	return "AgentCheckActiveTo();";
}
?>