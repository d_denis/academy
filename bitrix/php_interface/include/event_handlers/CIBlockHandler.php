<?
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("CIBlockHandler", "OnBeforeIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("CIBlockHandler", "OnBeforeIBlockElementDeleteHandler"));
class CIBlockHandler
{
    // создаем обработчик события "OnBeforeIBlockElementUpdate"
    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        if ($arFields["IBLOCK_ID"] == IBLOCK_NEWS_ID and $arFields["ACTIVE"] == "N") {
            global $DB;
            global $APPLICATION;
            $curDate = date_create_from_format("d.m.Y", date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT"))));
            date_sub($curDate, date_interval_create_from_date_string('3 days'));
            $arFilter = array(
                "IBLOCK_ID" => IBLOCK_NEWS_ID,
                "ACTIVE" => "Y",
                "ID" => array($arFields["ID"]),
                ">=DATE_ACTIVE_FROM" => date_format($curDate, "d.m.Y")
            );
            $arSelect = array("NAME", "DATE_ACTIVE_FROM");

            $res = CIBlockElement::GetList(
                array(),
                $arFilter,
                false,
                false,
                $arSelect
            );
            $ITEM = $res->GetNext();
            if (!empty($ITEM)) {
                $APPLICATION->throwException("Вы деактивировали свежую новость(ID:" . $arFields["ID"] . ")");
                return false;
            }

        }
    }

    // создаем обработчик события "OnBeforeIBlockElementDelete"
    function OnBeforeIBlockElementDeleteHandler($ID)
    {
        global $APPLICATION;
        $arFilter = array(
            "IBLOCK_ID" => IBLOCK_CAT_ID,
            "ID" => array($ID),
            ">SHOW_COUNTER" => "1"
        );
        $arSelect = array("NAME", "ACTIVE", "SHOW_COUNTER");

        $res = CIBlockElement::GetList(
            array(),
            $arFilter,
            false,
            false,
            $arSelect
        );
        $ITEM = $res->GetNext();

        if (!empty($ITEM)) {
            $el = new CIBlockElement;
            $res = $el->Update($ID, array("ACTIVE"=>"N"));
            $APPLICATION->throwException("Товар деактивирован, у него ".$ITEM["SHOW_COUNTER"]." просмотров.");
            $GLOBALS['DB']->Commit();
            return false;
        }

    }


}
?>