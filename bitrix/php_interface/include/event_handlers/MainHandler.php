<?
AddEventHandler("main", "OnBeforeUserUpdate", Array("MainHandler", "OnBeforeUserUpdateHandler"));
AddEventHandler("main", "OnAfterUserUpdate", Array("MainHandler", "OnAfterUserUpdateHandler"));
class MainHandler
{
    private static $USER_IN_GROUP = false;
    // создаем обработчик события "OnBeforeUserUpdate"
    function OnBeforeUserUpdateHandler(&$arFields)
    {
        foreach ($arFields["GROUP_ID"] as $group) {
            if ($group["GROUP_ID"] == GROUP_CONTENT_ID) {
                global $USER;
                if (!in_array(GROUP_CONTENT_ID, CUser::GetUserGroup($arFields["ID"]))) {
                    self::$USER_IN_GROUP = true;
                }
                break;
            }
        }
    }

    // создаем обработчик события "OnAfterUserUpdate"
    function OnAfterUserUpdateHandler(&$arFields)
    {
        if (self::$USER_IN_GROUP) {
            foreach ($arFields["GROUP_ID"] as $group) {
                if ($group["GROUP_ID"] == GROUP_CONTENT_ID) {
                    $arUsers = CGroup::GetGroupUser(GROUP_CONTENT_ID);
                    $arEmails = array();
                    foreach ($arUsers as $id) {
                        if ($id != $arFields['ID']) {
                            $user = CUser::GetByID($id)->Fetch();
                            array_push($arEmails, $user['EMAIL']);
                        }
                    }
                    if ($arEmails) {
                        $arEventFields = array(
                            "NAME" => $arFields["NAME"],
                            "EMAIL" => implode(", ", $arEmails)
                        );
                        CEvent::Send("NEW_USER_IN_GROUP", MY_SITE_ID, $arEventFields);
                    }
                    break;
                }
            }
        }
    }
}
?>